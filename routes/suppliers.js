const express = require("express");

// Import validator
const {
  createSuppliersValidator,
} = require("../middlewares/validators/suppliers");

// Import controller
const {
  getAllSupplier,
  getDetailSupplier,
  createSupplier,
  updateSupplier,
  deleteSupplier,
} = require("../controllers/suppliers");

const router = express.Router();

router
  .route("/")
  .post(createSuppliersValidator, createSupplier)
  .get(getAllSupplier);

router
  .route("/:id")
  .get(getDetailSupplier)
  .put(updateSupplier)
  .delete(deleteSupplier);

module.exports = router;
