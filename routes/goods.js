const express = require("express");

const {
  createGood,
  getAllGoods,
  getDetailGood,
  updateGood,
  deleteGood,
} = require("../controllers/goods");

const {
  createAndUpdateGoodValidator,
} = require("../middlewares/validators/goods");

const router = express.Router();

router.get("/", getAllGoods);
router.get("/:id", getDetailGood);
router.post("/", createAndUpdateGoodValidator, createGood);
router.put("/:id", createAndUpdateGoodValidator, updateGood);
router.delete("/:id", deleteGood);

module.exports = router;
