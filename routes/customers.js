const express = require("express");

// Import validator
const {
  createOrUpdateCustomersValidator,
} = require("../middlewares/validators/customers");

// Import controller
const {
  getAllCustomer,
  getDetailCustomer,
  createCustomer,
  updateCustomer,
  deleteCustomer,
} = require("../controllers/customers");

const router = express.Router();

router
  .route("/")
  .post(createOrUpdateCustomersValidator, createCustomer)
  .get(getAllCustomer);

router
  .route("/:id")
  .get(getDetailCustomer)
  .put(createOrUpdateCustomersValidator, updateCustomer)
  .delete(deleteCustomer);

module.exports = router;
