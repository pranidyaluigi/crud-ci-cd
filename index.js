const express = require("express"); // Import express
const fileUpload = require("express-fileupload");

// Import routes
const goods = require("./routes/goods");
const transactions = require("./routes/transactions");
const suppliers = require("./routes/suppliers");
const customers = require("./routes/customers");

const port = process.env.PORT || 3000; // Define port

const app = express();

// Enable req.body (json and urlencoded)
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

// Enable req.body (form-data)
app.use(fileUpload());

app.use(express.static("public"));

// Make routes
app.use("/goods", goods);
app.use("/transactions", transactions);
app.use("/suppliers", suppliers);
app.use("/customers", customers);
// Run the server
app.listen(port, () => console.log(`Server running on port ${port}`));
